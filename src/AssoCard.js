import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FacebookIcon from '@material-ui/icons/Facebook';
import {useFavorite} from "./Favorite";

const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: 345,
    },
    media: {
        width: "100%",
        paddingTop: '100%', // 16:9
        backgroundSize: "contain"
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

export default function AssoCard({association:a={}}) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const [isFavorite, toggleFavorite] = useFavorite(a.id);

    return (
        <Card className={classes.card}>
            <CardHeader
                title={a.name}
                subheader=""
            />
            <CardMedia
                className={classes.media}
                image={a.logo_url}
                title={a.name}
            />
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {expanded ? a.description : a.short_description}
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <IconButton aria-label="add to favorites" onClick={toggleFavorite}>
                    <FavoriteIcon color={isFavorite?"secondary":"disabled"}/>
                </IconButton>
                {
                    a.website_url !== "" && a.website_url ?
                        <a href={a.website_url} target={"_blank"}>
                    <IconButton aria-label="share">
                        {
                            a.website_url.startsWith("https://www.facebook.com") ?
                                <FacebookIcon/> : <ShareIcon/>
                        }
                    </IconButton> </a>: null
                }
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>
        </Card>
    );
}

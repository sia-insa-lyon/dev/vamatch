import {useEffect, useMemo, useState} from "react";
import axios from "axios";

let cachedAssociations = [];
let cacheLoading = true;

export default function useAssociations({url = "https://portail.asso-insa-lyon.fr/api/v1/directory/", filter} = {}){
    const [value, setAssociations] = useState(cachedAssociations);
    const [loading, setLoading] = useState(cacheLoading);

    useEffect(() => {
        axios.get(url)
            .then((r) => r.data)
            .then((list) => {
                cacheLoading = false;
                cachedAssociations = [...list];
                return list;
            })
            .then((list) => setAssociations(list))
            .then(() => setLoading(false));
        return () => setLoading(true);
    }, [url]);

    const associations = useMemo(() => value.filter(filter ? filter : (a) => a.is_active), [value, filter])

    return {
        associations: associations,
        loading
    }
}
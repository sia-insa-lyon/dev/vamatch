import React, {useState} from 'react';
import './App.scss';
import AssoCard from "./AssoCard";
import CircularProgress from "@material-ui/core/CircularProgress";
import Favorite, {FavoriteContext} from "./Favorite";
import useAssociations from "./useAssociations";

function App({url = "https://portail.asso-insa-lyon.fr/api/v1/directory/"}) {
    const [favorite] = useState(new Favorite());
    const {associations, loading} = useAssociations({url});

    return (
        <div id="Cards">
            {
                loading ?
                    <CircularProgress/>
                    :
                    <FavoriteContext.Provider value={favorite}>
                        {associations.map(a =>
                            <AssoCard association={a} key={a.id}/>
                        )}
                    </FavoriteContext.Provider>
            }
        </div>
    );
}

export default App;

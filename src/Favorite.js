import * as React from "react";
import {useContext} from "react";
import {useCallback} from "react";
import {useState} from "react";
import {useEffect} from "react";

export default class Favorite {

    token = "favorite";

    async toggleFavorite(id) {
        const value = !this.isFavorite(id);
        window.localStorage.setItem(this._key(id), value ? "yes" : "no");
        return value;
    }

    _key(id) {
        return this.token + "-" + id;
    }

    isFavorite(id) {
        return window.localStorage.getItem(this._key(id)) === "yes";
    }
}

export const FavoriteContext = React.createContext();
export function useFavorite(id) {
    const favorite = useContext(FavoriteContext);
    const [isFav, setFav] = useState(false);
    const toggle = useCallback(async () => setFav(await favorite.toggleFavorite(id)), [id, favorite, setFav]);
    useEffect(() => {
        if(favorite) {
            setFav(() => favorite.isFavorite(id))
        }
    }, [favorite, id, setFav]);
    return [isFav, toggle];
}